package edu.upc.damo.arrancadacotxedef;

import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withResourceName;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by aitor on 25/10/2016.
 */

public class arrancadacotxeInstrumentationTest {

    @Rule
    public ActivityTestRule<arrancadacotxe> activityTestRule =
            new ActivityTestRule<>(arrancadacotxe.class);

    @Test
    public void validate() {
        onView(withId(R.id.arrenca)).perform(click());
        onView(withId(R.id.model)).check(matches(withText("Tipus de cotxe: amb boto")));
        onView(withId(R.id.text)).check(matches(withText("Apa som-hi!!")));

        onView(withId(R.id.arrenca)).perform(click());
        onView(withId(R.id.model)).check(matches(withText("Tipus de cotxe: amb clau")));
        onView(withId(R.id.text)).check(matches(withText("Apa som-hi!!")));

        onView(withId(R.id.arrenca)).perform(click());
        onView(withId(R.id.model)).check(matches(withText("Tipus de cotxe: amb boto")));
        onView(withId(R.id.text)).check(matches(withText("Apa som-hi!!")));

        onView(withId(R.id.clean)).perform(click());
        onView(withId(R.id.model)).check(matches(withText("Tipus de cotxe:")));
        onView(withId(R.id.text)).check(matches(withText("Marxem?")));
    }
}
