package edu.upc.damo.arrancadacotxedef;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toolbar;

public class arrancadacotxe extends Activity {
    TextView text, model;
    Button arrenca, clean;
    ImageView image, arranc;
    Toolbar toolbar;
    MenuItem searchMenuItem;
    AlertDialog dialog;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrancadacotxe);
        Inicialitzacio_arrancadacotxe();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.capcalera_cotxe, menu);

        //MenuItem searchMenuItem = menu.findItem(R.id.action_cerca);
        searchMenuItem = menu.findItem(R.id.action_ajut);
        (menu.findItem(R.id.action_cerca)).setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                toolbar.setBackgroundColor(Color.WHITE);
                //Deshabilitar_buto();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                toolbar.setBackgroundColor(Color.BLUE);
                //Habilitar_buto();
                return true;
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cerca:
                //Snackbar.make(text, "Aplica aqui la teva funcio de creca", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //item.onActionViewExpanded();
                return true;
            case R.id.action_esborra_resultat:
                //Snackbar.make(text, "Aplica aqui la teva funcio de borrat", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return true;
            case R.id.action_ajut:
                //Log.v("LOG","action ajut");
                Mostrar_ajuda();
                //Log.v("LOG","return");

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void Inicialitzacio_toolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);
    }
    public void Inicialitzacio_arrancadacotxe() {
            // Init variables
        text = (TextView) findViewById(R.id.text);
        model = (TextView) findViewById(R.id.model);
        arrenca = (Button) findViewById(R.id.arrenca);
        clean = (Button) findViewById(R.id.clean);
        image = (ImageView) findViewById(R.id.cotxe);
        image.setImageResource(R.mipmap.cotxe);
        arranc = (ImageView) findViewById(R.id.arranc);
        Inicialitzacio_toolbar();
        Ini_dialog();

            // Listeners
        arrenca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Postaenmarxa(v);    //NouContingut, NO podem introduir codi perque no te visibilitat sobre les variables
            }
        });
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Neteja(v);
            }
        });

    }
    private void Ini_dialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);

        builder.setTitle("AJUDA");
        builder.setMessage("Aquesta aplicació serveix per aprendre el funcionament de la Polimorfia en Android.");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        dialog = builder.create();
    }
    public void Deshabilitar_buto(){
        //NOT WORKING
        //searchMenuItem.setVisible(false);
        toolbar.hideOverflowMenu();
    }
    public void Habilitar_buto(){
        //NOT WORKING
        //searchMenuItem.setVisible(true);
        toolbar.showOverflowMenu();
    }
    private void Mostrar_ajuda(){
        dialog.show();
    }

    //Class GestorArrancada
    public abstract class GestorArrancada{
        private final static int NOARRENCA = 0;
        private final static int ARRENCA = 1;
        abstract public int Arrancar();
    }
    private class AmbBoto extends GestorArrancada{
        @Override
        public int Arrancar(){
            return GestorArrancada.ARRENCA;
        }
    }
    private class AmbClau extends GestorArrancada{
        @Override
        public int Arrancar(){
            return GestorArrancada.ARRENCA;
        }
    }

    private GestorArrancada getInstanceCotxe(){
        if (i == 0) {
            arranc.setImageResource(R.mipmap.boto);
            model.setText("Tipus de cotxe: amb boto");
            i++;
            return new AmbBoto();
        }
        else {
            arranc.setImageResource(R.mipmap.clau);
            model.setText("Tipus de cotxe: amb clau");
            i--;
            return new AmbClau();
        }
    }
    public void Postaenmarxa(View v){   //D'aquesta manera, la propia class es qui modifica els seus propis atributs
        GestorArrancada ga = getInstanceCotxe();

        switch (ga.Arrancar()) { //Es necessario passar-li la View?? No la fem servir
            case GestorArrancada.ARRENCA:
                Arrenquem();
                break;
        }
    }

    public void Neteja(View v){
        text.setText("Marxem?");
        model.setText("Tipus de cotxe:");
        arranc.setImageResource(0);
    }
    public void Arrenquem(){
        text.setText("Apa som-hi!!");
    }
}
